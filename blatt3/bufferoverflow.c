// Compile: clang -o bufferoverflow bufferoverflow.c
#include <stdio.h>
#include <string.h>
int main(int argc, char** args) {
  char passok = 'F';
  char password[8];
  printf("Passwort: ");
  gets(password);
  if (!strcmp(password, "daspassw")) { passok = 'T'; }
  if (passok == 'T') { printf("%s", "Willkommen!\n"); }
  else { printf("%s", "Falsches Passwort!\n"); }
  return 0;
}

#include <stdio.h>
#include <stdint.h>

int main() {
    uint64_t rip, rbp, rsp;
    asm ("lea (%%rip),%%rax" : "=r" (rip));
    asm ("lea (%%rbp),%%rax" : "=r" (rbp));
    asm ("lea (%%rsp),%%rax" : "=r" (rsp));
    printf("rip:    %p\n", rip);
    printf("rbp:    %p\n", rbp);
    printf("rsp:    %p\n", rsp);
    return 0;
}
